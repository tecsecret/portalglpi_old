<?php
/**
 * Apply Thread Prefix
 * Copyright 2011 Starpaul20
 */

$l['applythreadprefix_info_name'] = "Apply Thread Prefix";
$l['applythreadprefix_info_desc'] = "Allows moderators to apply a thread prefix to threads without having to edit the post.";

$l['apply_thread_prefix'] = "Aplicar prefixo ao Topico";
$l['new_prefix'] = "Novo Prefixo:";
$l['prefix_note'] = "Por favor, note que qualquer prefixo que este topico tenha atualmente sera sobrescrito";
$l['no_prefix'] = "Sem Prefixo";
$l['nav_apply_prefix'] = "Aplicar prefixo ao Topico";

$l['thread_prefix_applied'] = "Prefixo aplicado";
$l['redirect_thread_prefix_applied'] = "O prefixo foi aplicado com sucesso ao t�pico. <br /> Agora voc� ser� redirecionado para o t�pico.";
$l['redirect_inline_thread_prefix_applied'] = "O prefixo foi aplicado com sucesso aos topicos selecionados. <br /> Voc� sera redirecionado para o forum.";
$l['error_invalidthread'] = "The specified thread does not exist.";
$l['error_invalidforum'] = "The specified forum does not exist.";
$l['error_inline_nothreadsselected'] = "Sorry, but you did not select any threads to perform inline moderation on, or your previous moderation session has expired (Automatically after 1 hour of inactivity). Please select some threads and try again.";
$l['error_no_prefixes'] = "There are no thread prefixes available.";
