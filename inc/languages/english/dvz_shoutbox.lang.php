<?php
/* by Tomasz 'Devilshakerz' Mlynski [devilshakerz.com]; Copyright (C) 2014-2016
 released under Creative Commons BY-NC-SA 4.0 license: https://creativecommons.org/licenses/by-nc-sa/4.0/ */

// server
$l['dvz_sb_shoutbox'] = 'CHAT';
$l['dvz_sb_archive'] = 'Arquivo do CHAT';
$l['dvz_sb_archivelink'] = 'arquivo';
$l['dvz_sb_last_read_link'] = 'Ir para a �ltima leitura';
$l['dvz_sb_last_read_unmark_all'] = 'Marcar tudo como lido';
$l['dvz_sb_default'] = 'Sua Mensagem aqui...';
$l['dvz_sb_user_blocked'] = 'Voc� foi bloqueado de postar mensagens no chat.';
$l['dvz_sb_minposts'] = 'Post at least {MINPOSTS} forum messages to be able to use Shoutbox.';
$l['dvz_sb_mod'] = 'Modera��o do CHAT';
$l['dvz_sb_mod_banlist'] = 'Banned users\' IDs (comma-separated)';
$l['dvz_sb_mod_banlist_button'] = 'Salvar';
$l['dvz_sb_mod_clear'] = 'Deletar mensagens mais antigas que...';
$l['dvz_sb_mod_clear_all'] = 'Todas as mensagens';
$l['dvz_sb_mod_clear_button'] = 'Deletar';
$l['dvz_sb_activity'] = 'Vendo <a href="%s">arquivo do chat</a>';

// JavaScript
$l['dvz_sb_delete_confirm'] = 'Tem certeza que deseja deletar essa mensagem?';
$l['dvz_sb_antiflood'] = 'Por favor, aguarde pelo menos {ANTIFLOOD} segundos entre as suas mensagens subsequentes.';
$l['dvz_sb_permissions'] = 'Voc� n�o tem permiss�o para executar esta a��o.';
