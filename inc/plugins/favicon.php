<?php
/**
 * MyBulletinBoard 1.8
 * Favicon 1.0
 * Author: Tomik
**/

if(!defined("IN_MYBB"))
{
	die('This file cannot be accessed directly.');
}

$plugins->add_hook("global_end", "favicon_insert");

function favicon_info()
{
	return array(
		"name"			=> "Favicon",
		"description"	=> "This plugin insert favicon in your community.",
		"website"		=> "https://www.pecetowicz.pl/",
		"author"		=> "Tomik",
		"codename"		=> "favicon",
		"authorsite"	=> "https://www.pecetowicz.pl/profil/tomasz-36285/",
		"version"		=> "1.0.1",
		"compatibility"	=> "18*"
	);
}
function favicon_activate()
{
require "../inc/adminfunctions_templates.php";
global $db;

$insertarray = array(
		"gid"			=> "NULL",
		"name"			=> "favicon",
		"title"			=> "Favicon",
		"description"	=> "Settings: Favicon.",
		"disporder"		=> "0",
		"isdefault"		=> "no",
	);
	$gid = $db->insert_query("settinggroups", $insertarray);

$favicon_setting_1 = array(
		"sid"			=> "NULL",
		"name"			=> "faviconyesno",
		"title"			=> "Enable plugin?",
		"description"	=> "",
		"optionscode"	=> "yesno",
		"value"			=> "no",
		"disporder"		=> "1",
		"gid"			=> intval($gid),
	);
	$db->insert_query("settings", $favicon_setting_1);

$favicon_setting_2 = array(
		"sid"			=> "NULL",
		"name"			=> "faviconurl",
		"title"			=> "Favicon",
		"description"	=> "Link to favicon.",
		"optionscode"	=> "text",
		"value"			=> "",
		"disporder"		=> "2",
		"gid"			=> intval($gid),
	);
	$db->insert_query("settings", $favicon_setting_2);
	
	rebuild_settings();
}
function favicon_deactivate()
{
	global $db;
	$db->delete_query("settings", "name IN('faviconyesno','faviconurl')");
	$db->delete_query("settinggroups", "name IN('favicon')");
}
function favicon_insert()
{
	global $headerinclude, $mybb;
	
	if(intval($mybb->settings['faviconyesno']) == 1)
	{
		$headerinclude = '<link rel="shortcut icon" href="'.$mybb->settings['faviconurl'].'" type="image/x-icon" />'. $headerinclude;
	}
}
?>