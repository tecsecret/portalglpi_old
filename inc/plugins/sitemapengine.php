<?php
/*
*
* SiteMap Engine Plugin
* Copyright 2015 mostafa shirali
* http://www.ctboard.com
* No one is authorized to redistribute or remove copyright without my expressed permission.
*
*/

if(!defined("IN_MYBB"))
{
	die("Direct initialization of this file is not allowed.<br /><br />Please make sure IN_MYBB is defined.");
}
$plugins->add_hook("index_end", "sitemap_do");
// The information that shows up on the plugin manager
function sitemapengine_info()
{
return array(
"name" => "sitemapengine",
"description" => "This plugin let you Create sitemap and update Automatically",
"website" => "http://ctboard.com",
"author" => "Mostafa shirali",
"authorsite" => "http://ctboard.com",
"version" => "1.0",
'codename'        => 'sitemapengine',
);
}

// This function runs when the plugin is activated.
function sitemapengine_activate()
{
	global $mybb, $db, $templates;
    require MYBB_ROOT.'/inc/adminfunctions_templates.php';
 $settings_group = array(
        "gid" => "",
        "name" => "sitemapengine",
        "title" => "sitemapengine",
        "description" => "sitemapengine settings",
        "disporder" => "88",
        "isdefault" => "0",
        );
    $db->insert_query("settinggroups", $settings_group);
    $gid = $db->insert_id();

	$setting_1 = array("sid" => "","name" => "sitemapengineenable","title" => "active","description" => "do you want active plugin?","optionscode" => "yesno","value" => "0","disporder" => 1,"gid" => intval($gid),);
    $setting_2 = array( 'sid'=> "",'name'=> 'sitemapenginefre','title'=> 'frequancy','description'	=> 'insert your sitemap frequancy.can insert always hourly daily weekly monthly yearly','optionscode'=> 'text','value'=> 'daily','disporder'=> 2,'gid'=> intval($gid),);

$db->insert_query("settings", $setting_1);
$db->insert_query("settings", $setting_2);

}

function sitemapengine_deactivate()
{
	global $mybb, $db, $templates;
    require MYBB_ROOT.'/inc/adminfunctions_templates.php';
	$db->query("DELETE FROM ".TABLE_PREFIX."settinggroups WHERE name='sitemapengine'");
	$db->query("DELETE FROM ".TABLE_PREFIX."settings WHERE name='sitemapengineenable'");
	$db->query("DELETE FROM ".TABLE_PREFIX."settings WHERE name='sitemapenginefre'");


}
function sitemap_do()
{
global $mybb, $db, $templates;

if($mybb->settings['sitemapengineenable'] == 1)
{
        
					$threads=$db->query("SELECT tid FROM ".TABLE_PREFIX."threads  ORDER BY tid DESC");
					$threads_num=$db->num_rows($threads);
					$content='';
					for($i=0;$i<$threads_num;$i++)
					{
					$threads_info=$db->fetch_array($threads);
					$threadslink = get_thread_link($threads_info['tid']);
					$content.='<url><loc>'.$mybb->settings['bburl'].'/'.$threadslink.'</loc>
					<changefreq>'.$mybb->settings['sitemapenginefre'].'</changefreq>
					</url>';
					}
					$sitemap="<?xml version=\"1.0\" encoding=\"UTF-8\"?>
					<urlset xmlns=\"http://www.google.com/schemas/sitemap/0.9\">".$content."</urlset>
					<!-- sitemap-generator-url=\"http://ctboard.com\" sitemap-generator-version=\"1.0\" -->";
					$handle = @fopen(MYBB_ROOT . 'sitemap.xml', 'w+');
					fwrite($handle, $sitemap);
					fclose($handle);

					
    

}
}

?>