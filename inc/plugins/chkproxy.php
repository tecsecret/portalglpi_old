<?php
/**
chkProxy (Checks registration IP against a list of known proxy/VPNs from GetIPIntel.net.)
By KuJoe (JMD.cc)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

// Don't allow direct initialization.
if (! defined('IN_MYBB')) {
	die('Nope.');
}

function chkProxy_info() {
	return array(
		"name"			=> "chkProxy",
		"description"	=> 'Checks registration IP against a list of known proxy/VPNs. If detected the user is blocked from registering. <i>Powered by <a href="https://getipintel.net/" target="_blank">GetIPIntel.net</a>',
		"website"		=> "http://www.jmd.cc",
		"author"		=> "KuJoe",
		"authorsite"	=> "http://www.jmd.cc",
		"version"		=> "1.0",
		'compatibility'	=> '18*',
		'codename'		=> 'chkproxy'
	);
}

$plugins->add_hook("member_register_agreement", "chkProxy");

function chkProxy_activate() {
	global $db;
	$me = chkProxy_info();

	$chkProxy_group = array(
		'name'			=> 'chkProxy',
		'title'			=> 'chkProxy',
		'description'	=> 'chkProxy Settings.',
		'disporder'		=> '99',
		'isdefault'		=> '0'
	);

	$db->insert_query('settinggroups', $chkProxy_group);
	$gid = $db->insert_id();

	$chkProxy_setting_1 = array(
		'name'			=> 'chkProxy_score',
		'title'			=> 'chkProxy Score',
		'description'	=> 'Likelihood of proxy (0.25 = 25%, 0.50 = 50%, 0.75 = 75%, 1.0 = 100%). Adjust as needed.',
		'optionscode'	=> 'numeric',
		'value'			=> '0.95',
		'disporder'		=> '1',
		'gid'			=> intval($gid)
	);
	
	$chkProxy_setting_2 = array(
		'name'			=> 'chkProxy_email',
		'title'			=> 'Contact e-mail',
		'description'	=> 'This is used by the service owner to contact you regarding issues with your lookups and to prevent abuse of their API.',
		'optionscode' 	=> 'text',
		'value'			=> 'contact@example.com',
		'disporder'		=> '2',
		'gid'			=> intval($gid)
	);
	
	$db->insert_query('settings', $chkProxy_setting_1);
	$db->insert_query('settings', $chkProxy_setting_2);
	
	rebuild_settings();
}

function chkProxy_deactivate() {
	global $db, $mybb;

	$db->delete_query('settinggroups', "name='chkProxy'");
	$db->delete_query('settings', "name IN ('chkProxy_score','chkProxy_email')");
	rebuild_settings();


}

function chkProxy() {
	global $session, $db, $mybb;
	$ipaddr = $session->ipaddress;
	$email = $mybb->settings['chkProxy_email'];
	if (filter_var($ipaddr, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6) != true) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, '5');
		curl_setopt($ch, CURLOPT_URL, "http://check.getipintel.net/check.php?ip=$ipaddr&contact=$email");
		$response=curl_exec($ch);
		curl_close($ch);
		if (is_numeric($response)) {
			if ($response >= $mybb->settings['chkProxy_score']) {
				error("You appear to be using a proxy/VPN. Please disconnect and try again. If you think this is a mistake please contact us.");
				exit;
			}
		} else {
			error("The return message received is new to us so please try again in a few minutes.");
			exit;
		}
	}
}

?>